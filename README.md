- Create a 'keys' folder in the current directory<br>
- Create ssh keys<br>
  ssh-keygen -t rsa -C "Jump host keys" -f ./keys/jump_host_key<br>
  ssh-keygen -t rsa -C "Admin keys" -f ./keys/admin_key<br>
