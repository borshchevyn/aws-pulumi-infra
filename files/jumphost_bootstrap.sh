#!/bin/bash

#install aws cli
#dnf update -y &&
sudo dnf install -y unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip
sudo ./aws/install

#add private key to ec2-user profile 
echo "jump_host_priv_key" > /home/ec2-user/.ssh/id_rsa && chown ec2-user: /home/ec2-user/.ssh/id_rsa && chmod 600 /home/ec2-user/.ssh/id_rsa

