import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";
import * as fs from "fs";

let vpcIgw: aws.ec2.InternetGateway;
let vpcNat: aws.ec2.NatGateway;
let vpcEip: aws.ec2.Eip;

let ec2WebSrv: aws.ec2.Instance[] = [];
let vpcPubSub: aws.ec2.Subnet[] = [];
let vpcPrivSub: aws.ec2.Subnet[] = [];
let vpcPrivateRT: aws.ec2.RouteTable;
let vpcPublicRT: aws.ec2.RouteTable;

//const amiImage: string = "ami-0b0af3577fe5e3532";
const amiImage = pulumi.output(
  aws.ec2.getAmi({
    filters: [
      {
        name: "name",
        values: ["RHEL*-*-x86_64-*"],
      },
      {
        name: "virtualization-type",
        values: ["hvm"],
      },
      {
        name: "root-device-type",
        values: ["ebs"],
      },
    ],
    owners: ["309956199498"], // This owner ID is RedHat
    //owners: ["137112412989"], // This owner ID is Amazon
    mostRecent: true,
  })
);

const instanceType: string = "t2.micro";
const webSrvCount: number = 2;
const projectRegionAZ = aws.getAvailabilityZones({
  state: "available",
});
const projectConfig = new pulumi.Config();
const projectName = pulumi.getProject();
const projectStack = pulumi.getStack();
const projectRegion = "eu-central-1";
const vpcName = "InfraTestVPC";
const vpcCidrBlock = "10.10.0.0/16";
const vpcPublicSubnets: string[] = ["10.10.11.0/24", "10.10.12.0/24"];
const vpcPrivateSubnets: string[] = ["10.10.21.0/24", "10.10.22.0/24"];
const adminIPAddresses: string[] = [
  "10.10.0.0/16",
  "0.0.0.0/0", //temporarily added
  "85.172.14.211/32",
];

//--------------------------------------------------------------------------------
// Create VPC
//--------------------------------------------------------------------------------
const vpc = new aws.ec2.Vpc(vpcName, {
  cidrBlock: vpcCidrBlock,
  tags: {
    Name: vpcName,
  },
});

//--------------------------------------------------------------------------------
// Create private & public route tables
//--------------------------------------------------------------------------------
vpcPublicRT = new aws.ec2.RouteTable(`${vpcName + "-public-RT"}`, {
  vpcId: vpc.id,
  tags: {
    Name: `${vpcName + "-public-RT"}`,
  },
});

vpcPrivateRT = new aws.ec2.RouteTable(`${vpcName + "-private-RT"}`, {
  vpcId: vpc.id,
  tags: {
    Name: `${vpcName + "-private-RT"}`,
  },
});

//--------------------------------------------------------------------------------
// Create internet gateway
//--------------------------------------------------------------------------------
vpcIgw = new aws.ec2.InternetGateway(`${vpcName + "-IGW"}`, {
  vpcId: vpc.id,
  tags: {
    Name: `${vpcName + "-IGW"}`,
  },
});

//--------------------------------------------------------------------------------
// Create elastic IP
//--------------------------------------------------------------------------------
vpcEip = new aws.ec2.Eip(
  `${vpcName + "-EIP"}`,
  {
    vpc: true,
    tags: {
      Name: `${vpcName + "-EIP"}`,
    },
  },
  {
    dependsOn: [vpcIgw],
  }
);

//--------------------------------------------------------------------------------
// Create public & private subnets
//--------------------------------------------------------------------------------
vpcPublicSubnets.forEach((net) =>
  vpcPubSub.push(
    new aws.ec2.Subnet(
      `${vpcName + "-public-" + vpcPublicSubnets.indexOf(net)}`,
      {
        vpcId: vpc.id,
        mapPublicIpOnLaunch: true,
        availabilityZone: projectRegionAZ.then(
          (available) => available.names[vpcPublicSubnets.indexOf(net)]
        ),

        cidrBlock: net,
        tags: {
          Name: `${vpcName + "-public"}`,
        },
      }
    )
  )
);

vpcPrivateSubnets.forEach((net) =>
  vpcPrivSub.push(
    new aws.ec2.Subnet(
      `${vpcName + "-private-" + vpcPrivateSubnets.indexOf(net)}`,
      {
        vpcId: vpc.id,
        availabilityZone: projectRegionAZ.then(
          (available) => available.names[vpcPrivateSubnets.indexOf(net)]
        ),

        cidrBlock: net,
        tags: {
          Name: `${vpcName + "-private"}`,
        },
      }
    )
  )
);

//--------------------------------------------------------------------------------
// Create NAT
//--------------------------------------------------------------------------------
vpcNat = new aws.ec2.NatGateway(
  `${vpcName + "-NAT"}`,
  {
    subnetId: vpcPubSub[0].id,
    allocationId: vpcEip.id,
    tags: {
      Name: `${vpcName + "-NAT"}`,
    },
  },
  {
    dependsOn: [vpcIgw],
  }
);

//--------------------------------------------------------------------------------
// Create route and route associations
//--------------------------------------------------------------------------------
const vpcPublicSubnetsDefault = new aws.ec2.Route(
  `${vpcName + "-publicNetDefault"}`,
  {
    routeTableId: vpcPublicRT.id,
    destinationCidrBlock: "0.0.0.0/0",
    gatewayId: vpcIgw.id,
  }
);

const vpcPrivateSubnetsDefault = new aws.ec2.Route(
  `${vpcName + "-privateNetDefault"}`,
  {
    routeTableId: vpcPrivateRT.id,
    destinationCidrBlock: "0.0.0.0/0",
    gatewayId: vpcNat.id,
  }
);

vpcPublicSubnets.forEach(
  (net) =>
    new aws.ec2.RouteTableAssociation(
      `${"routeTableAssociationPub-" + vpcPublicSubnets.indexOf(net)}`,
      {
        subnetId: vpcPubSub[vpcPublicSubnets.indexOf(net)].id,
        routeTableId: vpcPublicRT.id,
      }
    )
);

vpcPrivateSubnets.forEach(
  (net) =>
    new aws.ec2.RouteTableAssociation(
      `${"routeTableAssociationPriv-" + vpcPrivateSubnets.indexOf(net)}`,
      {
        subnetId: vpcPrivSub[vpcPrivateSubnets.indexOf(net)].id,
        routeTableId: vpcPrivateRT.id,
      }
    )
);

//--------------------------------------------------------------------------------
// Create security groups
//--------------------------------------------------------------------------------
const allowHttp = new aws.ec2.SecurityGroup("allowHttp", {
  description: "Allow HTTP/HTTPS incoming traffic",
  vpcId: vpc.id,
  ingress: [
    {
      fromPort: 80,
      toPort: 80,
      protocol: "tcp",
      cidrBlocks: ["0.0.0.0/0"],
    },
    {
      fromPort: 443,
      toPort: 443,
      protocol: "tcp",
      cidrBlocks: ["0.0.0.0/0"],
    },
  ],
  egress: [
    {
      fromPort: 0,
      toPort: 0,
      protocol: "-1",
      cidrBlocks: ["0.0.0.0/0"],
    },
  ],
  tags: {
    Name: "allowHttp",
  },
});

const allowSSH = new aws.ec2.SecurityGroup("allowSSH", {
  description: "Allow SSH incoming traffic",
  vpcId: vpc.id,
  ingress: [
    {
      fromPort: 22,
      toPort: 22,
      protocol: "tcp",
      cidrBlocks: adminIPAddresses,
    },
  ],
  egress: [
    {
      fromPort: 0,
      toPort: 0,
      protocol: "-1",
      cidrBlocks: ["0.0.0.0/0"],
    },
  ],
  tags: {
    Name: "allowSSH",
  },
});

//--------------------------------------------------------------------------------
// Create S3 bucket
//--------------------------------------------------------------------------------
const infraTestS3 = new aws.s3.Bucket("infraTestS3", {
  acl: "private",
  bucket: "infra-test-s3-dev-01",
  tags: {
    Name: "InfraTestS3_Dev",
  },
});

//--------------------------------------------------------------------------------
// Create IAM policy & roles
//--------------------------------------------------------------------------------
const s3ReadPolicy = new aws.iam.Policy(
  "s3ReadPolicy",
  {
    path: "/",
    description: "S3 read only access policy",
    policy: JSON.stringify({
      Version: "2012-10-17",
      Statement: [
        {
          Action: ["s3:Get*", "s3:List*"],
          Effect: "Allow",
          Resource: [
            "arn:aws:s3:::infra-test-s3-dev-01",
            "arn:aws:s3:::infra-test-s3-dev-01/*",
          ],
        },
      ],
    }),
  },
  {
    dependsOn: [infraTestS3],
  }
);

const s3WritePolicy = new aws.iam.Policy(
  "s3WritePolicy",
  {
    path: "/",
    description: "S3 write access policy",
    policy: JSON.stringify({
      Version: "2012-10-17",
      Statement: [
        {
          Action: ["s3:*Object", "s3:List*"],
          Effect: "Allow",
          Resource: [
            "arn:aws:s3:::infra-test-s3-dev-01",
            "arn:aws:s3:::infra-test-s3-dev-01/*",
          ],
        },
      ],
    }),
  },
  {
    dependsOn: [infraTestS3],
  }
);

const s3ReadRole = new aws.iam.Role("s3ReadRole", {
  assumeRolePolicy: JSON.stringify({
    Version: "2012-10-17",
    Statement: [
      {
        Effect: "Allow",
        Sid: "",
        Action: "sts:AssumeRole",
        Principal: {
          Service: "ec2.amazonaws.com",
        },
      },
    ],
  }),
});

const s3WriteRole = new aws.iam.Role("s3WriteRole", {
  assumeRolePolicy: JSON.stringify({
    Version: "2012-10-17",
    Statement: [
      {
        Action: "sts:AssumeRole",
        Effect: "Allow",
        Sid: "",
        Principal: {
          Service: "ec2.amazonaws.com",
        },
      },
    ],
  }),
});

const s3Read = new aws.iam.PolicyAttachment("s3Read", {
  roles: [s3ReadRole.name],
  policyArn: s3ReadPolicy.arn,
});

const s3Write = new aws.iam.PolicyAttachment("s3Write", {
  roles: [s3WriteRole.name],
  policyArn: s3WritePolicy.arn,
});

const s3ReadProfile = new aws.iam.InstanceProfile("s3ReadProfile", {
  role: s3ReadRole.name,
});

const s3WriteProfile = new aws.iam.InstanceProfile("s3WriteProfile", {
  role: s3WriteRole.name,
});

//--------------------------------------------------------------------------------
// Create EC2 instances
//--------------------------------------------------------------------------------
let pubKey = fs.readFileSync("./keys/admin_key.pub");
const adminPubKey = new aws.ec2.KeyPair("adminPubKey", {
  publicKey: pubKey.toString(),
});

pubKey = fs.readFileSync("./keys/jump_host_key.pub");
const jumpHostPubKey = new aws.ec2.KeyPair("jumpHostPubKey", {
  publicKey: pubKey.toString(),
});

let userData = fs.readFileSync("./files/jumphost_bootstrap.sh");
let privKey = fs.readFileSync("./keys/jump_host_key");
let bootStrapScript = userData
  .toString()
  .replace("jump_host_priv_key", privKey.toString());

const bastionHost = new aws.ec2.Instance("bst01", {
  ami: amiImage.id,
  userData: bootStrapScript,
  instanceType: instanceType,
  keyName: adminPubKey.keyName,
  associatePublicIpAddress: true,
  vpcSecurityGroupIds: [allowSSH.id],
  iamInstanceProfile: s3WriteProfile,
  subnetId: vpcPubSub[0].id,
  tags: {
    Name: "JumpHost",
  },
});

userData = fs.readFileSync("./files/websrv_bootstrap.sh");
for (let i = 1; i <= webSrvCount; i++) {
  ec2WebSrv[i - 1] = new aws.ec2.Instance(`${"web0" + i}`, {
    ami: amiImage.id,
    instanceType: instanceType,
    userData: userData.toString(),
    keyName: jumpHostPubKey.keyName,
    vpcSecurityGroupIds: [allowSSH.id, allowHttp.id],
    iamInstanceProfile: s3ReadProfile,
    subnetId: vpcPrivSub[i - 1].id,
    tags: {
      Name: "WebHost",
    },
  });
}

//--------------------------------------------------------------------------------
// Create LoadBalancer (application)
//--------------------------------------------------------------------------------
const infraTestLB = new aws.lb.LoadBalancer("infraTestLB", {
  name: "infraTestLB",
  internal: false,
  loadBalancerType: "application",
  subnets: vpcPubSub.map((vpcPubSub) => vpcPubSub.id),
  securityGroups: [allowHttp.id],
});

const infraTestTG = new aws.lb.TargetGroup("infraTestTG", {
  port: 80,
  protocol: "HTTP",
  vpcId: vpc.id,
  healthCheck: {
    path: "/index.html",
  },
});

ec2WebSrv.forEach(
  (webSrv) =>
    new aws.lb.TargetGroupAttachment(
      `${"infraTestTGA-" + ec2WebSrv.indexOf(webSrv)}`,
      {
        targetGroupArn: infraTestTG.arn,
        targetId: webSrv.id,
        //  port: 80,
      }
    )
);

const infraTestLisntener = new aws.lb.Listener("infraTestListener", {
  //name: "infraTestListener",
  loadBalancerArn: infraTestLB.arn,
  port: 80,
  protocol: "HTTP",
  defaultActions: [
    {
      type: "forward",
      targetGroupArn: infraTestTG.arn,
    },
  ],
});

//--------------------------------------------------------------------------------
// Export outputs
//--------------------------------------------------------------------------------
type WebSrvInfo = {
  id: string;
  privateIP: string;
};

export const infraVPCName = vpcName;
export const infraJumpHostIp = bastionHost.publicIp;
export const infraS3Name = infraTestS3.bucketDomainName;
export const infraWebSrvInfo: any[] = [];
export const infraLoadBalancerIP = infraTestLB.dnsName;

for (let i = 0; i < ec2WebSrv.length; i++) {
  infraWebSrvInfo[i] = ec2WebSrv[i].privateIp;
}
